# Init

Clone this repository, then run init to clone all others within it.

```sh
git clone git@gitlab.com:praktice/init.git praktice

./init.sh
```
