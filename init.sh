#!/bin/bash
source ./config.sh

for repo in "${REPOS[@]}"; do
  if [[ -d $repo ]]; then
    echo " [!] Skipping $repo, directory already exists."
    continue
  fi
    git clone git@gitlab.com:$GROUP/$repo $repo
done
